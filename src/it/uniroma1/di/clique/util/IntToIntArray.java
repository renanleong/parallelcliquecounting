package it.uniroma1.di.clique.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class IntToIntArray {

	public IntArray[] arrNodes = new IntArray[1];
	//private int used = 0;
	
	public void put(int key, IntArray value) {

		if (arrNodes == null)
			arrNodes = new IntArray[10];
		else if (arrNodes.length <= key)
			grow(key+1);
		
		//System.out.println("Key:" + key + " arrLength:" + arrNodes.length);

		arrNodes[key] = value;
	}	
	
	private void grow(int minCapacity) {
		// taken from ArrayList

		// overflow-conscious code
		int oldCapacity = arrNodes.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity < 0)
			newCapacity = minCapacity;

		// minCapacity is usually close to size, so this is a win:
		arrNodes = Arrays.copyOf(arrNodes, newCapacity);
	}
	
	public boolean containsKey(int key) {
		if (arrNodes.length <= key)
			return false;
		else
			return arrNodes[key] != null;
	}
	
	public IntArray get(int key) {
		if (arrNodes.length <= key)
			return null;
		else
			return arrNodes[key];
	}
	
	public int size() {
		return arrNodes.length;
	}
	
	public static void main(String[] args) {
		
		IntArray a = new IntArray();
		IntArray b = new IntArray();

		for (int i = 0; i < 5; i++) {
			a.add(i + 1);
			b.add(i * 2);
		}

		for (int i = 0; i < a.size(); i++) {
			System.out.println("a[" + i + "] = " + a.get(i));
			System.out.println("b[" + i + "] = " + b.get(i));
		}
				

		IntToIntArray map = new IntToIntArray();
		map.put(0, a);
		map.put(5, b);

		System.out.println("arrNodes lenght is " + map.arrNodes.length);

		for (int key = 0; key < map.arrNodes.length; key++){
			
			IntArray value = map.get(key);

			if (value == null)
				System.out.println("key " + key + " has a null value");
			else
				System.out.println(
						"Entry: " + key + " has an array of size " + value.size());

		}

	}
	
}
