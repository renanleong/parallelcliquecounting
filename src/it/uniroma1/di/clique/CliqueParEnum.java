package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class CliqueParEnum {

    protected IntToIntArray nodes = new IntToIntArray();
    public static CliqueParEnum instance;
    public int cliqueSize;
    public int max = 10000;
    
    private CliqueParEnum() {}

    public CliqueParEnum(String pathFile) throws IOException {

        instance = this;

        // parse the input file and add edges to the graph
        try {

            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line;
            while ((line = br.readLine()) != null) {

                String[] l = line.split(" ");
                if (l.length == 1)
                    l = line.split("\t");

                this.addEdge(l[0], l[1]);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long countCliquesOfSize(int cliqueSize, int parallelism, int expansionSize) {

    	// compute the total order ≺ over the nodes and sort neighbors lists
        prepareForCounting();

        this.cliqueSize = cliqueSize;
        		
        // create pool of threads
        ForkJoinPool fjPool = new ForkJoinPool(parallelism);

        // start computation
        return fjPool.invoke(new GraphCompute(null, null, null, -1, -1, expansionSize, 0));
    }

    public void addEdge(String a, String b) {

        int v = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    public void addEdge(int v, int y) {

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
        if (!nodes.containsKey(y)){
        	IntArray arr = new IntArray();
        	nodes.put(y, arr);
        }
    }

    public int getNodesNumber() {
        return nodes.size();
    }

    public IntArray getNeighbors(int node) {
        return nodes.get(node);
    }
    
    public int[] findSubset(long position, int size, int expansionSize){
    	
    	int[] subset = new int[expansionSize];
    	int element = 1;
    	int index = 1;
    	long lastPosition = 1;
    	
    	do{
    		lastPosition = 1;
    		
    		for (int i = 1, m = (size - element); i <= (expansionSize - index); i++, m--)
    			lastPosition = lastPosition * m / i;
    		if (position <= lastPosition){
    			subset[index-1] = element-1;
    			index++;
    		}
    		else
    			position = position - lastPosition;
    		
    		element++;
    	}while(index < expansionSize);
    	
    	subset[index-1] = element + ((int) position) - 2;
    	
    	//return index +1
    	return subset;
    }
    
    public boolean checkAdj(int[] subset, IntArray candidates){
    	
    	boolean adj = true;
    	
    	for (int j = 0; j < subset.length-1; j++) {
			for (int j2 = j+1; j2 < subset.length; j2++) {
				if (nodes.get(candidates.get(subset[j])) == null && 
						nodes.get(candidates.get(subset[j2])) == null){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) == null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) == null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
				else if(nodes.get(candidates.get(subset[j])) != null &&
						nodes.get(candidates.get(subset[j2])) != null &&
						!nodes.get(candidates.get(subset[j])).contains(candidates.get(subset[j2])) &&
						!nodes.get(candidates.get(subset[j2])).contains(candidates.get(subset[j]))){
					adj = false;
					break;
				}
			}
			if (!adj)
			break;
		}
    	
    	return adj;
    	
    }
    
    class GraphCompute extends RecursiveTask<Long> {

        private int[] vertices;
        public GraphCompute parent = null;
        public GraphCompute next = null;
        public IntArray candidates = null;
        private long lo;
        private long hi;
        private int expansionSize;
        private int depth;

        public GraphCompute(int[] vertices, GraphCompute parent, IntArray candidates, long lo, long hi, 
        		int expansionSize, int depth) {
            this.vertices = vertices;
            this.parent = parent;
            this.candidates = candidates;
            this.lo = lo;
            this.hi = hi;
            this.expansionSize = expansionSize;
            this.depth = depth;
        }

        @Override
        protected Long compute() {

            CliqueParEnum graph = CliqueParEnum.instance;
            int cliqueSize = graph.cliqueSize;
            GraphCompute prev = null;

            long count = 0;
            
            // Check if it is the first computation
            if (depth == 0){
            	
            	int newExpansion;
				if(expansionSize + 1 > cliqueSize)
					newExpansion = expansionSize - 1;
				else
					newExpansion = expansionSize;
				
				for(int v = 0; v < graph.getNodesNumber(); v++){
					IntArray list = graph.getNeighbors(v);
					
					if (list != null && list.size() >= cliqueSize - 1) {
						
						int subset[] = new int[expansionSize];
            			subset[0] = v;
            			
            			for (int i = 1; i < subset.length; i++) {
							subset[i] = -1;
						}
						
						long subsets = 1;
						for (int j = 1, m = list.size(); j <= newExpansion; j++, m--)
							subsets = subsets * m / j;
						
						if(subsets <= max){
							GraphCompute gc = new GraphCompute(subset, this, list, 1, subsets, newExpansion, 1);
			            	gc.next = prev;
			            	gc.fork();
							prev = gc;
						}
						else{
							long groups = (subsets/max) + ((subsets % max == 0) ? 0 : 1);
							for (int i = 1; i <= groups; i++) {
								long first = (i-1) * max + 1;
								GraphCompute gc = new GraphCompute(subset, this, list, first, i*max, newExpansion, 1);
				            	gc.next = prev;
				        		gc.fork();
								prev = gc;
							}
						}
					}
				}
            }
            else{
            	
            	boolean hasNeighbors, adj;
            	
            	int[] indicesSubset = findSubset(lo, candidates.size(), expansionSize);
            	
            	for (long i = lo; i <= hi; i++) {
            		
            		hasNeighbors = true;            	
            		adj = checkAdj(indicesSubset, candidates);
            		
            		if((depth + expansionSize == cliqueSize) && adj){
            			count++;
            			adj = false;
            		}
            	         
            		if(adj){
            			int subset[] = new int[expansionSize];
        			
           				IntArray nextCandidates = candidates;
        			
            			for (int j = 0; j < expansionSize; j++) {
            			
            				subset[j] = candidates.get(indicesSubset[j]);
            			
            				if (nextCandidates != null && nextCandidates.size() >= cliqueSize - (depth + expansionSize) 
            						&& nodes.get(subset[j]) != null && 
            						nodes.get(subset[j]).size() >= cliqueSize - (depth + expansionSize)){
            					nextCandidates = IntArray.intersection(nextCandidates, nodes.get(subset[j]));
            				}
            				else{
            					hasNeighbors = false;
            					break;
            				}
            			}
            				
            			if(hasNeighbors && nextCandidates != null && 
            				nextCandidates.size() >= cliqueSize - (depth + expansionSize)){

            				if(depth == cliqueSize - expansionSize - 1){
            					count += nextCandidates.size();
            				}
            				else{
            					int newExpansion;
            					if(depth + expansionSize > cliqueSize - expansionSize)
            						newExpansion = cliqueSize - depth - expansionSize;
            					else
            						newExpansion = expansionSize;
            						
            					long subsets = 1;
            					for (int j = 1, m = nextCandidates.size(); j <= newExpansion; j++, m--)
            						subsets = subsets * m / j;
            					
            					if(subsets < max){
            						// create new computations
            						GraphCompute gc = new GraphCompute(subset, this, nextCandidates, 
            							1, subsets, newExpansion, (depth + expansionSize));
            						gc.next = prev;
            						gc.fork();
            						prev = gc;
            					}
            					else{
            						long groups = (subsets/max) + ((subsets % max == 0) ? 0 : 1);
            						//System.out.println("Groups:" + groups);
            						
        							for (int j = 1; j <= groups; j++) {
        								long first = (j-1) * max + 1;
        								// create new computations
        								GraphCompute gc = new GraphCompute(subset, this, nextCandidates, first, 
        										j*max, newExpansion, (depth + expansionSize));
        				            	gc.next = prev;
        				            	gc.fork();
        								prev = gc;
        							}
            					}
            				}
            			}
            		}
            		
            		if ((indicesSubset[0]) == candidates.size() - expansionSize)
            			break;
            		
            		//expand
            		if (indicesSubset[expansionSize-1] < candidates.size()-1)
            			indicesSubset[expansionSize-1]++;
            		//reduce
            		else{
            			int j = 1;
            			while (indicesSubset[expansionSize - j - 1] == candidates.size() - j - 1)
            				j++;
            			indicesSubset[expansionSize - j - 1]++;
            			while (j > 0){
            				j--;
            				indicesSubset[expansionSize - j - 1] = indicesSubset[expansionSize - j - 2] + 1;
            			}
            		}
            	}
            	
            }
            //retrieve results from pending computations
        	while (prev != null) {
        		count += prev.join();
        		prev = prev.next;
        	}
        	
            return count;
        }
    }

    private void prepareForCounting() {

        CliqueParEnum gi = new CliqueParEnum();
        
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
            IntArray uList = this.getNeighbors(u);
            
            if(uList == null)
				continue;
            
            for (int i = 0; i < uList.size(); i++) {

                int v = uList.get(i);
                IntArray vList = this.getNeighbors(v);

                if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v)){
                    gi.addEdge(u, v);
                }
            }
        }

        this.nodes = gi.nodes;

        // sort neighbors list
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
         
			this.nodes.get(u).sort();
        }        
    }
}
