package it.uniroma1.di.clique;

import it.uniroma1.di.clique.util.IntArray;
import it.uniroma1.di.clique.util.IntToIntArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ThreeByThree implements CliqueCounterGraph {

    protected IntToIntArray nodes = new IntToIntArray();
    public static ThreeByThree instance;
    public int cliqueSize;
    
    private ThreeByThree() {}

    public ThreeByThree(String pathFile) throws IOException {

        instance = this;

        // parse the input file and add edges to the graph
        try {

            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line;
            while ((line = br.readLine()) != null) {

                String[] l = line.split(" ");
                if (l.length == 1)
                    l = line.split("\t");

                this.addEdge(l[0], l[1]);
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long countCliquesOfSize(int cliqueSize, int parallelism) {

    	// compute the total order ≺ over the nodes and sort neighbors lists
        prepareForCounting();

        this.cliqueSize = cliqueSize;
        
        // create pool of threads
        ForkJoinPool fjPool = new ForkJoinPool(parallelism);

        // start computation
        return fjPool.invoke(new GraphCompute(-1, -1, -1, null, null, 0));
    }

    @Override
    public void addEdge(String a, String b) {

        int v = Integer.parseInt(a);
        int y = Integer.parseInt(b);

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    public void addEdge(int v, int y) {

        if (nodes.containsKey(v)) {
            nodes.get(v).add(y);
        } else {
            IntArray arr = new IntArray();
            arr.add(y);
            nodes.put(v, arr);
        }
    }

    @Override
    public int getNodesNumber() {
        return nodes.size();
    }

    public IntArray getNeighbors(int node) {
        return nodes.get(node);
    }

    class GraphCompute extends RecursiveTask<Long> {

        private int first;
        private int second;
        private int third;
        public GraphCompute parent = null;
        public GraphCompute next = null;
        public IntArray candidates = null;
        private int depth;

        public GraphCompute(int first, int second, int third, GraphCompute parent, IntArray candidates, int depth) {
            this.first = first;
            this.second = second;
            this.third = third;
            this.parent = parent;
            this.candidates = candidates;
            this.depth = depth;
        }

        @Override
        protected Long compute() {

        	ThreeByThree graph = ThreeByThree.instance;
            int cliqueSize = graph.cliqueSize;
            GraphCompute prev = null;

            int counter = 0;
            long count = 0;
            
            // check if it is the first computation
            if (first < 0) {
            	
            	for(int u = 0; u < graph.getNodesNumber(); u++){ 
                	
                	// check if "u" has sufficient neighbors to complete a clique
                	if(nodes.get(u) == null || nodes.get(u).size() < cliqueSize - 1)
                		continue;
                	                	
                	IntArray neighbors = nodes.get(u);
                	
                	for(int i = 0; i < neighbors.size(); i++){
                		
                		int v = nodes.get(u).get(i);
                		
                		// check if "v" has sufficient neighbors to complete a clique
                		if(nodes.get(v) == null || nodes.get(v).size() < cliqueSize - 2)
                			continue;

                		// compute the intersection
                		IntArray firstIntersection = IntArray.intersection(nodes.get(u), nodes.get(v));
                		
                		// check if there are sufficient nodes to complete a clique in "firstIntersection"
                		if(firstIntersection == null || firstIntersection.size() < cliqueSize - 2)
                			continue;
                		
                		
                		if(cliqueSize == 3){
                			count += firstIntersection.size();
                		}
                		else{
                			for(int j = 0; j < firstIntersection.size(); j++){
                				
                				int x = firstIntersection.get(j);
                				
                				//System.out.println(u + " " + v + " " + x);
                				
                				// check if "x" has sufficient neighbors to complete a clique
                				if (nodes.get(x) == null || nodes.get(x).size() < cliqueSize -3)
                					continue;
                				
                				// compute the intersection
                				IntArray nextCandidates = IntArray.intersection(firstIntersection, nodes.get(x));
                				
                				// check if there are sufficient nodes to complete a clique in "nextCandidates"
                				if(nextCandidates == null || nextCandidates.size() < cliqueSize - 3)
                        			continue;
                				
                				if(cliqueSize == 4){
                					count += nextCandidates.size();                					
                				}
                				else{
                					// create new computations
                					GraphCompute gc = new GraphCompute(u, v, x, null, nextCandidates, 3);
                                    gc.next = prev;
                                    counter++;
                                    gc.fork();
                                    prev = gc;
                				}
                			}
                		}
                	}
                	//retrieve results from 1000000 pending computations
                	if(counter > 1000000){
                		while (prev != null) {
                    		count += prev.join();
                        	prev = prev.next;
                		}
                		prev = null;
                		counter = 0;
                	}
                }
                
                //retrieve results from pending computations
        		while (prev != null) {
            		count += prev.join();
                	prev = prev.next;
            	}
            } 
            else {
            	                
                for (int k = 0; k < candidates.size(); k++) {

                    int u = candidates.get(k);
                    
                    // check if "u" has sufficient neighbors to complete a clique
                    if(nodes.get(u) == null || nodes.get(u).size() < cliqueSize - (depth + 1))
                    	continue;
                    
                    // compute the intersection
                    IntArray firstIntersect = IntArray.intersection(candidates, nodes.get(u));
            		
                    // check if two nodes are missing to complete a clique ("u" + 1)
                    if (depth == cliqueSize - 2){
                		
                    	if(firstIntersect == null)
                    		continue;
                		
                		count += firstIntersect.size();
                    }
                	else if (depth == cliqueSize - 3){
                		
                		// check if there are sufficient nodes to complete a clique in "firstIntersection"
                		if(firstIntersect == null || firstIntersect.size() < cliqueSize - (depth + 1))
                    		continue;
                    
                    	for(int i = 0; i < firstIntersect.size(); i++){
                    		
                    		int v = firstIntersect.get(i);
                    		
                    		// check if "v" has sufficient neighbors to complete a clique
                    		if(nodes.get(v) == null || nodes.get(v).size() < cliqueSize - (depth + 2))
                               	continue;
                    		
                    		// compute the intersection
                    		IntArray secondIntersect = IntArray.intersection(firstIntersect, nodes.get(v));
                    		
                    		if(secondIntersect == null)
                               	continue;
                    		
                    		count += secondIntersect.size();
                    	}
                    }
                    else{
                    	
                    	// check if there are sufficient nodes to complete a clique in "firstIntersection"
                    	if(firstIntersect == null || firstIntersect.size() < cliqueSize - (depth + 1))
                    		continue;
                    	
                    	for(int i = 0; i < firstIntersect.size(); i++){
                    
                    		int v = firstIntersect.get(i);
                    		
                    		// check if "v" has sufficient neighbors to complete a clique
                    		if(nodes.get(v) == null || nodes.get(v).size() < cliqueSize - (depth + 2))
                        		continue;
                    		
//                    		increaseCounter();
                    		
                    		// compute the intersection
                    		IntArray secondIntersect = IntArray.intersection(firstIntersect, nodes.get(v));
                    		
                    		// check if there are sufficient nodes to complete a clique in "secondIntersection"
                    		if(secondIntersect == null || secondIntersect.size() < cliqueSize - (depth + 2))
                    			continue;
                    		
                    		for(int j = 0; j < secondIntersect.size(); j++){
                    			
                    			int x = secondIntersect.get(j);
                    			
                    			// check if "x" has sufficient neighbors to complete a clique
                    			if(nodes.get(x) == null || nodes.get(x).size() < cliqueSize - (depth + 3))
                            		continue;
                    			
                    			// compute the intersection
                        		IntArray thirdIntersect = IntArray.intersection(secondIntersect, nodes.get(x));
                        		
                        		// check if four nodes are missing to complete a clique ("u" + "v" + "x" + 1)
                        		if(depth == cliqueSize - 4){
                        			
                        			if (thirdIntersect == null)
                        				continue;
                        			
                        			count += thirdIntersect.size();
                        		}
                        		else{
                        			// check if there are sufficient nodes to complete a clique in "thirdIntersection"
                        			if(thirdIntersect == null || thirdIntersect.size() < cliqueSize - (depth + 3))
                            			continue;
                        			
                        			// create new computations
                        			GraphCompute gc = new GraphCompute(u, v, x, this, thirdIntersect, depth+3);
                               		gc.next = prev;
                                	gc.fork();
                                	prev = gc;
                        		}
                    		}
                    	}
                    }
                }

              //retrieve results from pending computations
                while (prev != null) {
                    count += prev.join();
                    prev = prev.next;
                }
            }
            return count;
        }

    }

    private void prepareForCounting() {

        ThreeByThree gi = new ThreeByThree();
        
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
        	IntArray uList = this.getNeighbors(u);
        	
        	if(uList == null)
				continue;
        	
            for (int i = 0; i < uList.size(); i++) {

                int v = uList.get(i);
                IntArray vList = this.getNeighbors(v);

                if (uList.size() < vList.size() || (uList.size() == vList.size() && u < v))
                    gi.addEdge(u, v);
            }
        }

        this.nodes = gi.nodes;

        // sort neighbors list
        for (int u = 0; u < this.nodes.arrNodes.length; u++){
			
			if(this.nodes.get(u) == null)
				continue;
         
			this.nodes.get(u).sort();
        }
    }
}
